package com.spider;

import com.spider.mapper.SpiderMapper;
import com.spider.pojo.Spider;
import com.spider.util.Assist;
import com.spider.util.CollectionUtils;
import com.spider.util.GreyCorrelation;
import com.spider.vo.RoomRelevanceVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Autowired
	SpiderMapper spiderMapper;
	@Test
	public void contextLoads() {
		Assist assist = new Assist();
//		assist.setOrder("ziroom_spider.price",true);
		assist.setRequires(Assist.andLt("ziroom_spider.price",2200),
				Assist.andGt("ziroom_spider.area",8.2),
				Assist.andNeq("nearestSubWayDist","null"));
		List<Spider> spiders = spiderMapper.selectSpider(assist);
		List<RoomRelevanceVo> roomRelevanceVos = new ArrayList<>();
		double lat = 30.325279;
		double lng = 120.227155;
		for (Spider spider : spiders){
			RoomRelevanceVo roomRelevanceVo = new RoomRelevanceVo();
			roomRelevanceVo.setId(spider.getId());
			roomRelevanceVo.setArea(spider.getArea());
			roomRelevanceVo.setLink(spider.getLink());
			roomRelevanceVo.setPrice(spider.getPrice());
			roomRelevanceVo.setTitle(spider.getTitle());
			roomRelevanceVo.setCoDistance(GreyCorrelation.GetDistance(lng,lat,spider.getLng(),spider.getLat()));
			roomRelevanceVos.add(roomRelevanceVo);

			System.out.println("lat = " + lat + " lng = " + lng + " lat1 =" +spider.getLat() + " lng1 = " +spider.getLng() + "  CoDistance = " + roomRelevanceVo.getCoDistance());
		}
		int colLength = 3;
		double[][] doubleList =  CollectionUtils.toArray(roomRelevanceVos,colLength);
		double[] doubles = GreyCorrelation.greyCorrelationMethod(doubleList);
		int count = 0;
		for (RoomRelevanceVo roomRelevanceVo : roomRelevanceVos){
			roomRelevanceVo.setRelevance(doubles[count]);
			count++;
		}
		RoomRelevanceVo[] roomRelevanceVos1 = CollectionUtils.listToArray(roomRelevanceVos, RoomRelevanceVo.class);
		Arrays.sort(roomRelevanceVos1);
		System.out.println(roomRelevanceVos1);
	}

}
