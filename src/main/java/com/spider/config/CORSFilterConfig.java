package com.spider.config;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Created by Ruan on 2017/9/12.
 */
@Component
public class CORSFilterConfig implements Filter {

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

//        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));//这里填写你允许进行跨域的主机ip
        response.addHeader("Access-Control-Allow-Origin", "*");
//        response.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");//允许的访问方法
        response.setHeader("Access-Control-Max-Age", "3600");//Access-Control-Max-Age 用于 CORS 相关配置的缓存
        response.setHeader("Access-Control-Allow-Headers",  "Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With");

        response.setHeader("Access-Control-Allow-Credentials","true"); //是否支持cookie跨域`
        response.setHeader("Content-Type", "application/x-www-form-urlencoded");//允许的访问方法

        chain.doFilter(req, res);

    }

    public void init(FilterConfig filterConfig) {}

    public void destroy() {}

}
