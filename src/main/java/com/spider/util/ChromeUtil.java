package com.spider.util;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * Chrome游览器工具类
 */
public class ChromeUtil {

    /**
     * 设置窗口大小
     * @param chromeOptions
     */
    public static void buildWindowSize(ChromeOptions chromeOptions) {
        Map<String, Object> deviceMetrics = new HashMap<>();
        deviceMetrics.put("width", 360);

        deviceMetrics.put("height", 640);

        deviceMetrics.put("pixelRatio", 3.0);

        Map<String, Object> mobileEmulation = new HashMap<>();

        mobileEmulation.put("deviceMetrics", deviceMetrics);
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);

    }


    public static WebDriver creatLog() {
        LoggingPreferences logs = new LoggingPreferences();
        logs.enable(LogType.BROWSER, Level.ALL);
        logs.enable(LogType.CLIENT, Level.ALL);
        logs.enable(LogType.DRIVER, Level.ALL);
        logs.enable(LogType.PERFORMANCE, Level.ALL);
        logs.enable(LogType.PROFILER, Level.ALL);
        logs.enable(LogType.SERVER, Level.ALL);

        DesiredCapabilities desiredCapabilities = DesiredCapabilities.firefox();
        desiredCapabilities.setCapability(CapabilityType.LOGGING_PREFS, logs);


        return new ChromeDriver(desiredCapabilities);
    }

    /**
     * 创建新的窗口
     * @param targetUrl
     * @param driver
     */
    public static void creatNewWindows(String targetUrl, WebDriver driver) {
//        Actions actionOpenLinkInNewTab = new Actions(driver);

//        actionOpenLinkInNewTab.keyDown(Keys.CONTROL).sendKeys("t").keyUp(Keys.CONTROL).perform();

        System.out.println("创建新窗口");

        Actions actions = new Actions(driver);
        actions.keyDown(Keys.COMMAND).sendKeys("t").keyUp(Keys.COMMAND).build().perform();

    }

    /**
     * 静态页面等待
     * @param time 毫秒
     * @param driver
     */
    public static void await(long time, WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(time, TimeUnit.MILLISECONDS);
    }


    public static WebElement findElementByText(WebDriver driver, String text, String selector) {
        List<WebElement> list = driver.findElements(By.cssSelector(selector));
        for (WebElement webElement : list) {
            if (text.equals(webElement.getText())) {
                return webElement;
            }
        }
        return null;
    }


    public static LocalDateTime computeSellTime(String sellTimeStr) {

        String[] split = sellTimeStr.split("：");

        //需要加上年才能转换
        String time = LocalDate.now().getYear() + "-" + split[1];

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM月dd日 HH:mm");
        return LocalDateTime.parse(time, formatter).minusNanos(800 * 1000000);
    }

    public static void main(String[] args) {

        System.out.println(LocalDateTime.now().toString());
        String times = "开售时间：09月08日 13:02";

        LocalDateTime delay = computeSellTime(times);

        ScheduledThreadPoolExecutor scheduled = new ScheduledThreadPoolExecutor(1);
        scheduled.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println(LocalDateTime.now().toString());
            }
        },Duration.between(LocalDateTime.now(), delay).toMillis() ,  TimeUnit.MILLISECONDS);
    }
}
