package com.spider.spider.annotation;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.model.AfterExtractor;
import us.codecraft.webmagic.model.annotation.ExtractBy;
import us.codecraft.webmagic.model.annotation.ExtractByUrl;
import us.codecraft.webmagic.model.annotation.HelpUrl;
import us.codecraft.webmagic.model.annotation.TargetUrl;

/**
 * Created by Ruan on 2017/9/10.
 */
@TargetUrl("http://hz.ziroom.com/z/vr/*")
@HelpUrl("http://hz.ziroom.com/z/nl/z2.html?p=\\d+")
public class ZiRoomRespo implements AfterExtractor {

    @ExtractBy("//div[@class='room_name']/h2/text()")
    private String title="";//标题

    @ExtractByUrl
    private String link="";//链接

    @ExtractBy("/html/body/div[3]/div[2]/div[1]/p/span[2]/span[1]/text()")
    private String price="";//价格

    @ExtractBy("//div[@class='room_name']/p/span[2]/span[2]/regex('.*\\((.+)\\).*',1)")
    private String time_unit="";//价格对应的时间单位（每天，每月）

    @ExtractBy("/html/body/div[3]/div[2]/ul/li[1]/regex('[\\s\\S]*?(\\d+(?:\\.\\d+)?).*㎡.*',1)")
    private String area="";//面积

    @ExtractBy(value = "/html/body/div[3]/div[2]/ul/li[3]/regex('(\\d+).*',1)",multi = true)
    private String rooms;//几居室

    @ExtractBy(value = "/html/body/div[3]/div[2]/ul/li[3]/regex('.*(\\d+).*',1)",multi = true)
    private String halls="";//几厅

    @ExtractBy("//ul[@class='detail_room']/li[3]/span/text()")
    private String rentType="";//租借类型(合，整，直)

    @ExtractBy("//ul[@class='detail_room']/li[4]/regex('.*/(\\d+).*$',1)")
    private String floorTotal="";//大楼总层数

    @ExtractBy("//ul[@class='detail_room']/li[4]/regex('(\\d+)/.*',1)")
    private String floorLoc="";//房间所在楼层

    @ExtractBy("//*[@id=\"mapsearchText\"]/@data-lng")
    private String lng="";//房间所在经度

    @ExtractBy("//*[@id=\"mapsearchText\"]/@data-lat")
    private String lat="";//房间所在维度

    @ExtractBy("/html/body/div[3]/div[2]/ul/li[2]/text()")
    private String direction="";//房屋朝向

    @ExtractBy("/html/body/div[3]/div[2]/p/a/span/regex('.*?(\\d+\\.?\\d*).*',1)")
    private String confGen="";//第几代配置

    @ExtractBy("/html/body/div[3]/div[2]/p/a/span/regex('.*?\\d+\\.?\\d* *(.*)$',1)")
    private String confType="";//配置风格(布丁，木棉等)

    @ExtractBy("/html/body/div[3]/div[2]/p/span[@class=\"toilet\"]/text()")
    private String privateBathroom="";//是否有独立卫生间(0:否,1:是)

    @ExtractBy("/html/body/div[3]/div[2]/p/span[@class=\"balcony\"]/text()")
    private String privateBalcony="";//是否有独立阳台(0:否,1:是)

    @ExtractBy("//ul[@class='detail_room']/li[5]/span/regex('.*?(\\d+)米.*',1)")
    private String nearestSubWayDist="";//最近地铁距离(m)

    @ExtractBy("/html/body/div[3]/div[2]/div[1]/p/span[1]/regex('.*?\\[(.+?) .*',1)")
    private String district="";//地区


    @Override
    public String toString() {
        return "ZiRoomRespo{" +
                "floorTotal='" + floorTotal + '\'' +
                ", rooms=" + rooms +
                ", lng='" + lng + '\'' +
                ", direction='" + direction + '\'' +
                ", floorLoc='" + floorLoc + '\'' +
                ", halls='" + halls + '\'' +
                ", rentType='" + rentType + '\'' +
                ", time_unit='" + time_unit + '\'' +
                ", title='" + title + '\'' +
                ", privateBathroom='" + privateBathroom + '\'' +
                ", district='" + district + '\'' +
                ", lat='" + lat + '\'' +
                ", area='" + area + '\'' +
                ", privateBalcony='" + privateBalcony + '\'' +
                ", confType='" + confType + '\'' +
                ", link='" + link + '\'' +
                ", confGen='" + confGen + '\'' +
                ", price='" + price + '\'' +
                ", nearestSubWayDist='" + nearestSubWayDist + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTime_unit() {
        return time_unit;
    }

    public void setTime_unit(String time_unit) {
        this.time_unit = time_unit;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRooms() {
        return rooms;
    }

    public void setRooms(String rooms) {
        this.rooms = rooms;
    }

    public String getHalls() {
        return halls;
    }

    public void setHalls(String halls) {
        this.halls = halls;
    }

    public String getRentType() {
        return rentType;
    }

    public void setRentType(String rentType) {
        this.rentType = rentType;
    }

    public String getFloorTotal() {
        return floorTotal;
    }

    public void setFloorTotal(String floorTotal) {
        this.floorTotal = floorTotal;
    }

    public String getFloorLoc() {
        return floorLoc;
    }

    public void setFloorLoc(String floorLoc) {
        this.floorLoc = floorLoc;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getConfGen() {
        return confGen;
    }

    public void setConfGen(String confGen) {
        this.confGen = confGen;
    }

    public String getConfType() {
        return confType;
    }

    public void setConfType(String confType) {
        this.confType = confType;
    }

    public String getPrivateBathroom() {
        return privateBathroom;
    }

    public void setPrivateBathroom(String privateBathroom) {
        this.privateBathroom = privateBathroom;
    }

    public String getPrivateBalcony() {
        return privateBalcony;
    }

    public void setPrivateBalcony(String privateBalcony) {
        this.privateBalcony = privateBalcony;
    }

    public String getNearestSubWayDist() {
        return nearestSubWayDist;
    }

    public void setNearestSubWayDist(String nearestSubWayDist) {
        this.nearestSubWayDist = nearestSubWayDist;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    @Override
    public void afterProcess(Page page) {

    }

}
