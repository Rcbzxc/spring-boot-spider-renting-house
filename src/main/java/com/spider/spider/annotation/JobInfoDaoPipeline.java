package com.spider.spider.annotation;

import com.spider.mapper.SpiderMapper;
import com.spider.pojo.Spider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.PageModelPipeline;

/**
 * Created by Ruan on 2017/9/10.
 */
@Component("JobInfoDaoPipeline")
public class JobInfoDaoPipeline implements PageModelPipeline<ZiRoomRespo> {

    @Autowired
    private SpiderMapper spiderMapper;

    @Override
    public void process(ZiRoomRespo ziRoomRespo, Task task) {
        ziRoomRespo.setConfType(ziRoomRespo.getConfType().substring(0,ziRoomRespo.getConfType().indexOf("<")));
        ziRoomRespo.setPrice(ziRoomRespo.getPrice().replaceAll("[^0-9]",""));

        System.out.println(ziRoomRespo.toString());

        if (!ziRoomRespo.getNearestSubWayDist().equals("")){
            Spider spider = getSpider(ziRoomRespo);
            spiderMapper.insertSpider(spider);
//            System.out.println(spider);
        }
    }
    private Spider getSpider(ZiRoomRespo ziRoomRespo){
        Spider spider = new Spider();
        spider.setTitle(ziRoomRespo.getTitle());
        spider.setLink(ziRoomRespo.getLink());
        spider.setPrice(Integer.valueOf(ziRoomRespo.getPrice()));
        spider.setTimeUnit(ziRoomRespo.getTime_unit());
        spider.setArea(Double.parseDouble(ziRoomRespo.getArea()));
        spider.setRooms(Integer.valueOf(ziRoomRespo.getRooms()));
        spider.setHalls(Integer.valueOf(ziRoomRespo.getHalls()));
        spider.setRentType(ziRoomRespo.getRentType());
        spider.setFloorTotal(Integer.valueOf(ziRoomRespo.getFloorTotal()));
        spider.setFloorloc(Integer.valueOf(ziRoomRespo.getFloorLoc()));
        spider.setLng(Double.parseDouble(ziRoomRespo.getLng()));
        spider.setLat(Double.parseDouble(ziRoomRespo.getLat()));
        spider.setDirection(ziRoomRespo.getDirection());
        spider.setConfgen(ziRoomRespo.getConfGen());
        spider.setConftype(ziRoomRespo.getConfType());
        if (ziRoomRespo.getPrivateBathroom().equals("")) {
            spider.setPrivatebathroom(0);//没有独卫
        }else
            spider.setPrivatebathroom(1);//有独卫
        if (ziRoomRespo.getPrivateBalcony().equals("免物业费")) {
            spider.setPrivatebalcony(0);//没有独立阳台
        }else
            spider.setPrivatebalcony(1);//有独立阳台
        spider.setNearestsubwaydist(Integer.valueOf(ziRoomRespo.getNearestSubWayDist()));
        spider.setDistrict(ziRoomRespo.getDistrict());
        return spider;
    }
}

