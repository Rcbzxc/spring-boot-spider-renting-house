package com.spider.spider;

import com.spider.mapper.SpiderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.model.OOSpider;

import javax.annotation.PostConstruct;

/**
 * Created by Ruan on 2017/9/12.
 */
//@Component
public class JobCrawler {
    @Autowired
    private SpiderMapper spiderMapper;

    @PostConstruct
    public void start() {
        OOSpider.create(new ZiRoomJSProcessor(spiderMapper))
                .addUrl("http://hz.ziroom.com/z/nl/-z2.html?p=1")
                .thread(1)
                .run();
    }

}
