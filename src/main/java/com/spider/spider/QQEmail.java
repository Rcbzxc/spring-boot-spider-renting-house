package com.spider.spider;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class QQEmail {
    public static void main(String[] args) {

        QQAccount.accountMaps.forEach((o,v)->{
            try {
                Thread.sleep(5000);
                loginQQEmail(o, v);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    private static void loginQQEmail(String account, String pwd) {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Administrator\\Desktop\\chromedriver_win32\\chromedriver.exe");//chromedriver服务地址

        Map<String, Object> deviceMetrics = new HashMap<>();
        deviceMetrics.put("width", 360);

        deviceMetrics.put("height", 640);

        deviceMetrics.put("pixelRatio", 3.0);

        Map<String, Object> mobileEmulation = new HashMap<>();

        mobileEmulation.put("deviceMetrics", deviceMetrics);

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);

        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.get("https://mail.qq.com/");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.findElement(By.id("u")).sendKeys(new String[]{account});
        driver.findElement(By.id("p")).sendKeys(new String[]{pwd});//找到u元素的id，然后输入hello

        driver.findElement(By.id("go")).click(); //点击按扭

        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();

    }
}
