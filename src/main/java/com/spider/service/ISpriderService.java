package com.spider.service;

import com.spider.common.ServerResponse;

/**
 * Created by Ruan on 2017/9/12.
 */
public interface ISpriderService {
    ServerResponse<?> getMap();

    ServerResponse<?> getRentSurvey();

    ServerResponse<?> getMessage(double lat, double lng, double price, double area);
}
