package com.spider.service.impl;

import com.google.common.collect.Maps;
import com.spider.common.ServerResponse;
import com.spider.mapper.SpiderMapper;
import com.spider.pojo.Spider;
import com.spider.service.ISpriderService;
import com.spider.util.Assist;
import com.spider.util.CollectionUtils;
import com.spider.util.GreyCorrelation;
import com.spider.vo.RentSurveyVo;
import com.spider.vo.RoomRelevanceVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Ruan on 2017/9/12.
 */
@Service
public class SpriderServiceImpl implements ISpriderService {
    @Autowired
    private SpiderMapper spiderMapper;

    public ServerResponse<?> getMap() {
        Assist assist = new Assist();
//        assist.setOrder("ziroom_spider.price",true);
//        assist.setRowSize(400);
//        assist.setStartRow(1);
        List<Spider> tampSpider = spiderMapper.selectSpider(assist);
//        Set<Spider> spiders = new HashSet<>(tampSpider);
        List<Map> maps = new ArrayList<>();
        for (Spider spider : tampSpider){
            Map map = Maps.newHashMap();
            int count = 0;
            map.put("lng",spider.getLng());
            map.put("lat",spider.getLat());
            if (spider.getPrice()<2000)
                count = 5;
            else if (spider.getPrice()>=2000 && spider.getPrice()<3000)
                count = 10;
            else
                count = 15;
            map.put("count",count);
            maps.add(map);
        }
        return ServerResponse.createBySuccess(maps);
    }

    @Override
    public ServerResponse<?> getRentSurvey() {
        Assist assist = new Assist();
//        assist.setOrder("ziroom_spider.price",true);
        List<Spider> spiders = spiderMapper.selectSpider(assist);
        RentSurveyVo rentSurvey = spiderMapper.getRentSurvey();
        int medPrice = 0;
        if(spiders.size()%2==0){
            medPrice = (spiders.get(spiders.size()/2).getPrice() + spiders.get((spiders.size()/2)+1).getPrice())/2;
        }else {
            medPrice = spiders.get((spiders.size()+1)/2).getPrice();
        }
        rentSurvey.setMedPrice(medPrice);
        rentSurvey.setCount(spiders.size());
        rentSurvey.setPriceList(getSpider(spiders));
        Map map = Maps.newHashMap();

        return ServerResponse.createBySuccess(rentSurvey);
    }

    @Override
    public ServerResponse<?> getMessage(double lat, double lng, double price, double area) {
        Assist assist = new Assist();
        assist.setRequires(Assist.andLt("ziroom_spider.price",price),
                Assist.andGt("ziroom_spider.area",area),
                Assist.andNeq("nearestSubWayDist","null"));
        List<Spider> spiders = spiderMapper.selectSpider(assist);
        List<RoomRelevanceVo> roomRelevanceVos = new ArrayList<>();
//        double lat = 30.325279;
//        double lng = 120.227155;
        for (Spider spider : spiders){
            RoomRelevanceVo roomRelevanceVo = new RoomRelevanceVo();
            roomRelevanceVo.setId(spider.getId());
            roomRelevanceVo.setArea(spider.getArea());
            roomRelevanceVo.setLink(spider.getLink());
            roomRelevanceVo.setPrice(spider.getPrice());
            roomRelevanceVo.setTitle(spider.getTitle());
            roomRelevanceVo.setCoDistance(GreyCorrelation.GetDistance(lng,lat,spider.getLng(),spider.getLat()));
            roomRelevanceVos.add(roomRelevanceVo);
//            System.out.println("lat = " + lat + " lng = " + lng + " lat1 =" +spider.getLat() + " lng1 = " +spider.getLng() + "  CoDistance = " + roomRelevanceVo.getCoDistance());
        }
        int colLength = 3;
        double[][] doubleList =  CollectionUtils.toArray(roomRelevanceVos,colLength);
        double[] doubles = GreyCorrelation.greyCorrelationMethod(doubleList);
        int count = 0;
        for (RoomRelevanceVo roomRelevanceVo : roomRelevanceVos){
            roomRelevanceVo.setRelevance(doubles[count]);
            count++;
        }
        RoomRelevanceVo[] roomRelevanceVos1 = CollectionUtils.listToArray(roomRelevanceVos, RoomRelevanceVo.class);
        Arrays.sort(roomRelevanceVos1);
        return ServerResponse.createBySuccess(roomRelevanceVos1);
    }

    private Integer [] getSpider(List<Spider> spiders){
        Integer [] prices = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        Integer [] baseDatas = {100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000, 3100, 3200, 3300, 3400, 3500, 3600, 3700, 3800, 3900, 4000, 4100, 4200, 4300, 4400, 4500, 4600, 4700, 4800, 4900, 5000};
        for (Spider spider : spiders){
            for (int i = 0; i < baseDatas.length; i++) {
                if (i == 0){
                    if (spider.getPrice() < baseDatas[i]){
                        prices[i]++;
                        break;
                    }
                }else {
                    if (baseDatas[i-1]<= spider.getPrice() && baseDatas[i]>spider.getPrice()){
                        prices[i]++;
                        break;
                    }
                }
            }
        }
        return prices;
    }
}
