package com.spider.controller;

import com.spider.common.ServerResponse;
import com.spider.service.ISpriderService;
import com.spider.service.impl.SpriderServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * Created by Ruan on 2017/9/12.
 */
@RestController
@RequestMapping("/ziroom/")
public class SpriderController {

    @Autowired
    private ISpriderService iSpriderService;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping(value = "map")
    public ServerResponse<?> map(HttpSession session){
        System.out.println("OK");
        return iSpriderService.getMap();
    }

    @GetMapping(value = "rentSurvey")
    public ServerResponse<?>rentSurvey(HttpSession session){
        return iSpriderService.getRentSurvey();
    }

    @GetMapping(value = "message")
    public ServerResponse<?>message(@RequestParam(value = "lat",required = false) double lat,
                                    @RequestParam(value = "lng",required = false)double lng,
                                    @RequestParam(value = "price",defaultValue = "2200")double price,
                                    @RequestParam(value = "area",defaultValue = "8.2")double area){
        return iSpriderService.getMessage(lat,lng,price,area);
    }
}
