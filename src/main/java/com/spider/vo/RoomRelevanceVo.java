package com.spider.vo;

import java.io.Serializable;

/**
 * Created by Ruan on 2017/9/15.
 */
public class RoomRelevanceVo implements Comparable<RoomRelevanceVo>{

    private Integer id;
    private String title;//标题
    private String link;//链接
    private Integer price;//价格
    private double area;//面积
    private double coDistance;//公司距离
    private double relevance;//关联系数
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getCoDistance() {
        return coDistance;
    }

    public void setCoDistance(double coDistance) {
        this.coDistance = coDistance;
    }

    public double getRelevance() {
        return relevance;
    }

    public void setRelevance(double relevance) {
        this.relevance = relevance;
    }

    public RoomRelevanceVo() {

    }

    @Override
    public String toString() {
        return "RoomRelevanceVo{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", price=" + price +
                ", area=" + area +
                ", coDistance=" + coDistance +
                ", relevance=" + relevance +
                '}';
    }

    @Override
    public int compareTo(RoomRelevanceVo o) {
        if (this.getRelevance() > o.getRelevance())
            return -1;
        else if (this.getRelevance() < o.getRelevance()){
            return 1;
        }
        return 0;
    }
}
