package com.spider.vo;

/**
 * Created by Ruan on 2017/9/13.
 */
public class RentSurveyVo {
    Integer count;//数量
    Integer maxPrice;//最大价格
    Integer minPrice;//最小价格
    Integer avgPrice;//平均价格
    Integer medPrice;//中位价格
    Integer[] priceList;//价格汇总
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(Integer avgPrice) {
        this.avgPrice = avgPrice;
    }

    public Integer getMedPrice() {
        return medPrice;
    }

    public void setMedPrice(Integer medPrice) {
        this.medPrice = medPrice;
    }

    public Integer[] getPriceList() {
        return priceList;
    }

    public void setPriceList(Integer[] priceList) {
        this.priceList = priceList;
    }

    public RentSurveyVo() {
    }

}
