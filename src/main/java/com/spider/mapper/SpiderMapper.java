package com.spider.mapper;
import com.spider.pojo.Spider;
import java.util.List;
import com.spider.util.Assist;
import com.spider.vo.RentSurveyVo;
import org.apache.ibatis.annotations.Param;
public interface SpiderMapper{
	/**
	 * 获得Spider数据的总行数,可以通过辅助工具Assist进行条件查询,如果没有条件则传入null
	 * @param assist
	 * @return
	 */
    long getSpiderRowCount(Assist assist);
	/**
	 * 获得Spider数据集合,可以通过辅助工具Assist进行条件查询,如果没有条件则传入null
	 * @param assist
	 * @return
	 */
    List<Spider> selectSpider(Assist assist);
	/**
	 * 获得一个Spider对象,以参数Spider对象中不为空的属性作为条件进行查询
	 * @param obj
	 * @return
	 */
    Spider selectSpiderByObj(Spider obj);
	/**
	 * 通过Spider的id获得Spider对象
	 * @param id
	 * @return
	 */
    Spider selectSpiderById(Integer id);
	/**
	 * 插入Spider到数据库,包括null值
	 * @param value
	 * @return
	 */
    int insertSpider(Spider value);
	/**
	 * 插入Spider中属性值不为null的数据到数据库
	 * @param value
	 * @return
	 */
    int insertNonEmptySpider(Spider value);
	/**
	 * 通过Spider的id删除Spider
	 * @param id
	 * @return
	 */
    int deleteSpiderById(Integer id);
	/**
	 * 通过辅助工具Assist的条件删除Spider
	 * @param assist
	 * @return
	 */
    int deleteSpider(Assist assist);
	/**
	 * 通过Spider的id更新Spider中的数据,包括null值
	 * @param enti
	 * @return
	 */
    int updateSpiderById(Spider enti);
 	/**
	 * 通过辅助工具Assist的条件更新Spider中的数据,包括null值
	 * @param value
	 * @param assist
	 * @return
	 */
    int updateSpider(@Param("enti") Spider value, @Param("assist") Assist assist);
	/**
	 * 通过Spider的id更新Spider中属性不为null的数据
	 * @param enti
	 * @return
	 */
    int updateNonEmptySpiderById(Spider enti);
 	/**
	 * 通过辅助工具Assist的条件更新Spider中属性不为null的数据
	 * @param value
	 * @param assist
	 * @return
	 */
    int updateNonEmptySpider(@Param("enti") Spider value, @Param("assist") Assist assist);

	RentSurveyVo getRentSurvey();
}