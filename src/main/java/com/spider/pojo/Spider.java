package com.spider.pojo;
public class Spider {
    private Integer id;
    private String title;//标题
    private String link;//链接
    private Integer price;//价格
    private String timeUnit;//价格对应的时间单位（每天，每月）
    private double area;//面积
    private Integer rooms;//几居室
    private Integer halls;//几厅
    private String rentType;//租借类型(合，整，直)
    private Integer floorTotal;//大楼总层数
    private Integer floorloc;//房间所在楼层
    private double lng;//房间所在经度
    private double lat;//房间所在维度
    private String direction;//房屋朝向
    private String confgen;//第几代配置
    private String conftype;//配置风格(布丁，木棉等)
    private Integer privatebathroom;//是否有独立卫生间(0:否,1:是)
    private Integer privatebalcony;//是否有独立阳台(0:否,1:是)
    private Integer nearestsubwaydist;//最近地铁距离(m)
    private String district;//地区
    private java.util.Date gmtCreate;
    private java.util.Date gmtModify;
    public Spider() {
        super();
    }
    public Spider(Integer id,String title,String link,Integer price,String timeUnit,double area,Integer rooms,Integer halls,String rentType,Integer floorTotal,Integer floorloc,double lng,double lat,String direction,String confgen,String conftype,Integer privatebathroom,Integer privatebalcony,Integer nearestsubwaydist,String district,java.util.Date gmtCreate,java.util.Date gmtModify) {
        super();
        this.id = id;
        this.title = title;
        this.link = link;
        this.price = price;
        this.timeUnit = timeUnit;
        this.area = area;
        this.rooms = rooms;
        this.halls = halls;
        this.rentType = rentType;
        this.floorTotal = floorTotal;
        this.floorloc = floorloc;
        this.lng = lng;
        this.lat = lat;
        this.direction = direction;
        this.confgen = confgen;
        this.conftype = conftype;
        this.privatebathroom = privatebathroom;
        this.privatebalcony = privatebalcony;
        this.nearestsubwaydist = nearestsubwaydist;
        this.district = district;
        this.gmtCreate = gmtCreate;
        this.gmtModify = gmtModify;
    }
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Integer getPrice() {
        return this.price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getTimeUnit() {
        return this.timeUnit;
    }

    public void setTimeUnit(String timeUnit) {
        this.timeUnit = timeUnit;
    }

    public double getArea() {
        return this.area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public Integer getRooms() {
        return this.rooms;
    }

    public void setRooms(Integer rooms) {
        this.rooms = rooms;
    }

    public Integer getHalls() {
        return this.halls;
    }

    public void setHalls(Integer halls) {
        this.halls = halls;
    }

    public String getRentType() {
        return this.rentType;
    }

    public void setRentType(String rentType) {
        this.rentType = rentType;
    }

    public Integer getFloorTotal() {
        return this.floorTotal;
    }

    public void setFloorTotal(Integer floorTotal) {
        this.floorTotal = floorTotal;
    }

    public Integer getFloorloc() {
        return this.floorloc;
    }

    public void setFloorloc(Integer floorloc) {
        this.floorloc = floorloc;
    }

    public double getLng() {
        return this.lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return this.lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getDirection() {
        return this.direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getConfgen() {
        return this.confgen;
    }

    public void setConfgen(String confgen) {
        this.confgen = confgen;
    }

    public String getConftype() {
        return this.conftype;
    }

    public void setConftype(String conftype) {
        this.conftype = conftype;
    }

    public Integer getPrivatebathroom() {
        return this.privatebathroom;
    }

    public void setPrivatebathroom(Integer privatebathroom) {
        this.privatebathroom = privatebathroom;
    }

    public Integer getPrivatebalcony() {
        return this.privatebalcony;
    }

    public void setPrivatebalcony(Integer privatebalcony) {
        this.privatebalcony = privatebalcony;
    }

    public Integer getNearestsubwaydist() {
        return this.nearestsubwaydist;
    }

    public void setNearestsubwaydist(Integer nearestsubwaydist) {
        this.nearestsubwaydist = nearestsubwaydist;
    }

    public String getDistrict() {
        return this.district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public java.util.Date getGmtCreate() {
        return this.gmtCreate;
    }

    public void setGmtCreate(java.util.Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public java.util.Date getGmtModify() {
        return this.gmtModify;
    }

    public void setGmtModify(java.util.Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    @Override
    public String toString() {
        return "Spider{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", price=" + price +
                ", timeUnit='" + timeUnit + '\'' +
                ", area=" + area +
                ", rooms=" + rooms +
                ", halls=" + halls +
                ", rentType='" + rentType + '\'' +
                ", floorTotal=" + floorTotal +
                ", floorloc=" + floorloc +
                ", lng=" + lng +
                ", lat=" + lat +
                ", direction='" + direction + '\'' +
                ", confgen='" + confgen + '\'' +
                ", conftype='" + conftype + '\'' +
                ", privatebathroom=" + privatebathroom +
                ", privatebalcony=" + privatebalcony +
                ", nearestsubwaydist=" + nearestsubwaydist +
                ", district='" + district + '\'' +
                '}';
    }
}
