## 项目介绍
- 租房系统是基于java语言开发。 使用SpringBoot整合开源框架。
- 使用Maven对项目进行模块化管理，提高项目的易开发性、扩展性。
- 系统包括4个子系统：公共功能、Web业务、爬虫、数据库
- 公共功能：公共功能（灰色关联算法、指标无量纲化、测距等等）、工具类
## 技术选型
    ● 核心框架：SpringBoot 1.5.3
    ● 持久层框架：MyBatis 3.4.5
    ● 爬虫框架：Webmagic 0.7.3
    ● 数据库连接池：Alibaba Druid 1.0.11
    ● 日志管理：SLF4J、Log4j2
    ● 前端框架：echort JS + Jquery
## 选择困难症
毕业到现在已经两年多了,之前一直呆在家乡这边工作,如今想去大城市闯闯,进入一个陌生的环境租房是一个令人头疼的事情。从茫茫房海中找一间中意的房间，实在是一件费时费力的活，对于我这种买东西直奔目的地的人来说，在这么多房间里对比各种房间属性选出最优的根本就是一种折磨（这里不得不说一下自如网的选房功能，列表筛选无法准备知道房间地点，而地图找房功能的筛选项又太少，实在无法满足我的需求）

我萌生了把“自如”所有在租的房间数据都爬下来，找出最符合我预期的房间的想法，制作好一套流程以后再要换租的时候就可以无脑操作了。

## 爬取数据
分析之前需要从自如网上爬数据，我用的是码云开源项目上的Webmagic(黄亿华）爬虫框架。这个框架上手挺快的，一开始我采用注解的方式获取数据，发现房间的数量比较少，找了下原因发现是自如的房间列表页中有些房间条目是js动态生成的，由于资料里只提供了基本的抓取前段渲染的页面，所以我只能放弃注解的方式重新编写了基本的爬虫代码。最终采到了所有在租的自如房间数据，共2325条。数据格式如下：
```    {
"id":16827,
"title":" 铭雅苑西区5居室-01卧 ",
"link":"http://hz.ziroom.com/z/vr/60690487.html",
"price":2190,
"time_unit":"季付",
"area":26.7,
"rooms":5,
"halls":1,
"rent_type":"合",
"floor_total":5,
"floorLoc":5,
"lng":120.089887,
"lat":30.343836,
"direction":"朝向： 南",
"confGen":"4.0",
"confType":null,
"privateBathroom":1,
"privateBalcony":0,
"nearestSubWayDist":null,
"district":"余杭",
"gmt_create":"2017/9/16 17:23:44",
"gmt_modify":"2017/9/16 17:23:44"
},
{
"id":16828,
"title":" 竹海水韵4居室-01卧 ",
"link":"http://hz.ziroom.com/z/vr/60662966.html",
"price":1590,
"time_unit":"季付",
"area":14.6,
"rooms":4,
"halls":1,
"rent_type":"合",
"floor_total":18,
"floorLoc":12,
"lng":119.982947,
"lat":30.242810,
"direction":"朝向： 南",
"confGen":"4.0",
"confType":null,
"privateBathroom":1,
"privateBalcony":1,
"nearestSubWayDist":null,
"district":"余杭",
"gmt_create":"2017/9/16 17:23:44",
"gmt_modify":"2017/9/16 17:23:44"
}
```
## 租金的整体印象

我只关心合租房的数据，再做脏数据过滤，共得到2311条合租房数据。合租房房租的平均值和中位数非常接近，整体数据基本无偏，即低价位和高价位的房间数量差不多。（网页显示在“租金的整体印象.html"）
![image](https://git.oschina.net/Rcbzxc/spring-boot-spider-renting-house/raw/master/img/impress.png)

## 租金地图

房间价格在地图上的分布如图。红色表示大于3000元/月的房间，绿色表示2000-3000元/月的房间，紫色表示小于2000元/月的房间。颜色越深表示同一个位置重叠有越多房屋。（网页显示在“合租房价格地理位置分布.html"）
![image](https://git.oschina.net/Rcbzxc/spring-boot-spider-renting-house/raw/master/img/map.png)

## 终极目标
最后，回到本次分析的终极目标，找出最符合我预期的房间。我需要做的就是综合我自己的关注属性值对房间进行排序，我最关心的房间属性是[房间面积，房间租金，到公司的距离]。
这里我使用的是灰度关联分析法对房间进行打分，详细计算过程网上都有，这里我就不再罗列了。

首先我过滤掉属性值超出我心理预期范围的房间，将价格大于等于2200元/月，面积小于等于8m2的房间过滤掉。过滤后的数据集中选5条数据如下：


房间编号|房间面积(m2)|房间租金(元)|到公司的距离(m)  
---|--- |---|---|
1 | 8.5 | 1,490 | 21,006.99
2 | 9.5 | 2,130 | 12,011.88
3 | 14 | 1,760 | 25,967.01
4 | 8.4 | 1,960 | 25,684.91
5 | 14.01 | 2,130| 24,231.67

对这三个属性值无量纲化，这里我使用的是离差标准化，如下。标准化后的取值范围为[0,1]。
![image](https://git.oschina.net/Rcbzxc/spring-boot-spider-renting-house/raw/master/img/math.png)

无量纲化后数据如下：

房间编号|房间面积无量纲化|房间租金无量纲化|到公司的距离无量纲化
---|--- |---|---|
1 | 0.017825312 | 0  | 0.644573716
2 | 0.196078431 | 1 | 0
3 | 0.998217469 | 0.421875 | 1
4 | 0 | 0.734375 | 0.979785212
5 | 1 | 1 | 0.875648597

然后设定最优序列，最理想的状态当然是房间面积最大，租金最小，到公司距离最近了。因此最优序列为[1,0,0]，计算每个属性与最优序列相应属性之间的关联系数如下：

房间编号|房间面积关联系数|房间租金关联系数|到公司的距离关联系数
---|--- |---|---|
1 | 0.337342153 | 1  | 0.43684386
2 | 0.383458647 | 0.333333333 | 1
3 | 0.996447602 | 0.542372881 | 0.333333333
4 | 0.333333333 | 0.405063291 | 0.337886874
5 | 1 | 0.333333333 | 0.363464915


由于我对不同属性的关注程度不同，因此这里需要设定每个属性的权重，权重值使用目标优化矩阵确定。

房间属性|房间面积|房间租金|到公司的距离|合计|调整
---|--- |---|---|---|---|
房间面积 | | 0 | 0 | 0 | 1
房间租金 | 1 |  | 0 | 1| 2
到公司的距离 | 1 | 1 | |2|3

因此房间面积权重为1/6，房间租金权重为1/3，到公司的距离权重为1/2，则每个房间的关联系数=房间面积关联系数/6+房间租金关联系数/3+到公司的距离关联系数/2，计算结果如下：

房间编号 | 房间关联系数 
---|---|
1|0.607978955
1|0.675020886
1|0.513532227
1|0.35952009
1|0.459510235

计算出所有房间的关联系数，从大到小排列后取Top20如下（网页显示在“终极目标.html"）：
![image](https://git.oschina.net/Rcbzxc/spring-boot-spider-renting-house/raw/master/img/target.png)

网页界面做的有点low，等有时间了在做的好看些！这下可以挑选房间的范围就大大缩小了，以后要是再租房感觉烦恼少好多。当然由于自如网上房间信息变动较快，随时都会有人下定，这套流程还得随租随用，不然筛选出房间后过个两三天再看，说不定房间早就被别人抢了。

## 常见的问题
1、程序要想开启爬虫，把sprider文件下的
JobCrawler.java @Component 注释给去掉

2、公式可以参考目录下的excel。
## 关于我
邮箱：1051845334@qq.com

QQ：1051845334
